{
	"name": "haskero",
	"displayName": "Haskero",
	"description": "A full featured Haskell IDE",
	"author": "Julien Vannesson",
	"license": "MIT",
	"version": "1.3.1",
	"publisher": "Vans",
	"homepage": "https://gitlab.com/vannnns/haskero/blob/master/README.md",
	"repository": {
		"type": "git",
		"url": "https://gitlab.com/vannnns/haskero"
	},
	"bugs": {
		"url": "https://gitlab.com/vannnns/haskero/issues"
	},
	"icon": "images/haskero_128px.png",
	"galleryBanner": {
		"color": "#038FD6",
		"theme": "dark"
	},
	"engines": {
		"vscode": "^1.1.36"
	},
	"categories": [
		"Programming Languages",
		"Snippets",
		"Extension Packs"
	],
	"keywords": [
		"haskell",
		"language server",
		"stack",
		"cabal",
		"intero"
	],
	"extensionDependencies": [
		"justusadam.language-haskell",
		"jcanero.hoogle-vscode"
	],
	"activationEvents": [
		"onLanguage:haskell"
	],
	"contributes": {
		"commands": [
			{
				"command": "haskero.insertType",
				"title": "Insert type",
				"category": "haskell"
			},
			{
				"command": "haskero.selectTarget",
				"title": "Select target",
				"category": "haskell"
			},
			{
				"command": "haskero.evaluate",
				"title": "Evaluate",
				"category": "haskell"
			},
			{
				"command": "haskero.evaluateFromInput",
				"title": "Evaluate From Input",
				"category": "haskell"
			},
			{
				"command": "haskero.evaluateTestCases",
				"title": "Evaluate Test Cases",
				"category": "haskell"
			}
		],
		"configuration": {
			"title": "Haskero configuration",
			"properties": {
				"haskero.intero.ignoreDotGhci": {
					"type": "boolean",
					"default": true,
					"description": "Ignore '.ghci' config files when launching intero ghci"
				},
				"haskero.intero.startupParams": {
					"type": "array",
					"default": [
						"--no-build",
						"--no-load"
					],
					"description": "(Warning, can break Haskero behavior) Parameters sent to intero ghci"
				},
				"haskero.intero.stackPath": {
					"type": "string",
					"default": "stack",
					"description": "(Warning, can break Haskero behavior) path to the stack executable."
				},
				"haskero.intero.ghciOptions": {
					"type": "array",
					"default": [
						"-Wall"
					],
					"description": "(Warning, can break Haskero behavior) Parameters sent to intero ghci via --ghci-options"
				},
				"haskero.maxAutoCompletionDetails": {
					"type": "number",
					"default": 100,
					"description": "Maximum auto completion information requests sent to get type info/module in the autocompletion flow. Set to 0 to disable details info in autocompletion."
				},
				"haskero.debugMode": {
					"type": "boolean",
					"default": false,
					"description": "Active debug mode. Can slow down haskero."
				}
			}
		}
	},
	"main": "./out/src/extension",
	"scripts": {
		"vscode:prepublish": "./node_modules/typescript/bin/tsc -p ./",
		"compile": "./node_modules/typescript/bin/tsc -watch -p ./",
		"update-vscode": "node ./node_modules/vscode/bin/install",
		"postinstall": "node ./node_modules/vscode/bin/install"
	},
	"devDependencies": {
		"@types/mocha": "^2.2.33",
		"@types/node": "^12.11.0",
		"typescript": "^3.6.4",
		"@types/vscode": "^1.1.36",
		"vscode": "^1.1.36"
	},
	"dependencies": {
		"vscode-languageclient": "^3.2.0",
		"vscode-test-adapter-api": "latest",
		"vscode-test-adapter-util": "latest"
	},
	"__metadata": {
		"id": "c8674456-3345-4bbd-9be3-b5a45e470f51",
		"publisherDisplayName": "Vans",
		"publisherId": "4e47d164-7cfb-4d7d-85c8-35973f6955e6"
	}
}
