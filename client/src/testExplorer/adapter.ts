import * as vscode from 'vscode';
import {
	TestAdapter,
	TestLoadStartedEvent,
	TestLoadFinishedEvent,
	TestRunStartedEvent,
	TestRunFinishedEvent,
	TestSuiteEvent,
	TestEvent
} from 'vscode-test-adapter-api';
import {Log} from 'vscode-test-adapter-util';
import {HaskeroClient} from "../utils/haskeroClient";
import {Handler} from "./handler";

type StatsEmitterType = TestRunStartedEvent | TestRunFinishedEvent | TestSuiteEvent | TestEvent;
type TestEmitterType = TestLoadStartedEvent | TestLoadFinishedEvent;

export class HaskeroAdapter implements TestAdapter {
	private disposables: {dispose(): void}[] = [];
	private handler: Handler;

	private readonly testsEmitter = new vscode.EventEmitter<TestEmitterType>();
	private readonly testStatesEmitter = new vscode.EventEmitter<StatsEmitterType>();
	private readonly autorunEmitter = new vscode.EventEmitter<void>();

	get tests(): vscode.Event<TestEmitterType> { return this.testsEmitter.event; }
	get testStates(): vscode.Event<StatsEmitterType> { return this.testStatesEmitter.event; }
	get autorun(): vscode.Event<void> | undefined { return this.autorunEmitter.event; }

	constructor(
	    private readonly haskeroClient: HaskeroClient,
		private readonly log: Log
	) {
        this.handler = new Handler(haskeroClient);
		this.log.info('Initializing haskero adapter');
		this.disposables.push(...[
            this.testsEmitter,
            this.testStatesEmitter,
            this.autorunEmitter,
        ]);
	}

	async load(): Promise<void> {
		this.log.info('Loading haskero tests');
		this.testsEmitter.fire({type: 'started'});
		const loadedTests = await this.handler.load();
		this.testsEmitter.fire({type: 'finished', suite: loadedTests});
	}

	async run(tests: string[]): Promise<void> {
		this.log.info(`Running haskero tests ${JSON.stringify(tests)}`);
		this.testStatesEmitter.fire({type: 'started', tests});
		await this.handler.run(tests, this.testStatesEmitter);
		this.testStatesEmitter.fire({type: 'finished'});
	}

	cancel(): void {}

	dispose(): void {
		this.cancel();
		for (const disposable of this.disposables) {
			disposable.dispose();
		}
		this.disposables = [];
	}
}
