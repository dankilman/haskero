import * as vscode from 'vscode';
import {
    TestEvent, TestInfo,
    TestRunFinishedEvent,
    TestRunStartedEvent,
    TestSuiteEvent,
    TestSuiteInfo
} from 'vscode-test-adapter-api';
import {HaskeroClient} from "../utils/haskeroClient";
import * as model from "../commands/evaluate/model";
import {EvaluateContentProvider, scheme} from "../commands/evaluate/provider";
import code from "../commands/evaluate/code";


type EmitterType = vscode.EventEmitter<
    TestRunStartedEvent
    | TestRunFinishedEvent
    | TestSuiteEvent
    | TestEvent
    >;


interface TestSuiteInfoInner extends TestSuiteInfo {
    readonly path: string[];
}

interface TestInfoInner extends TestInfo {
    readonly path: string[];
}

type ItemType = TestSuiteInfoInner | TestInfoInner;


export class Handler {
    constructor(private readonly haskeroClient: HaskeroClient) {}

    private async request(onlyLoad: boolean, tests?: string[]): Promise<model.EvaluationResponse> {
        if (tests && tests.length == 1 && tests[0] == 'root') {
            tests = null;
        }
        const evaluationRequests = new model.EvaluationRequests(
            code.getText(),
            true,
            scheme,
            onlyLoad,
            tests
        );
        return this.haskeroClient.client.sendRequest<model.EvaluationResponse>(
            'evaluate', evaluationRequests
        );
    }

    async load(): Promise<TestSuiteInfo> {
        const file = code.getCurrentFilename();

        const response = await this.request(true);

        const root: TestSuiteInfoInner = {
            type: 'suite',
            id: 'root',
            label: 'haskero',
            children: [],
            file,
            path: []
        };

        const suites = response.suites;
        const suitesMap = new Map<string, TestSuiteInfoInner>();
        for (const suite of suites) {
            const suitePath = suite.path;
            const suiteLabel = suitePath[suitePath.length - 1];
            const suiteId = suitePath.join('.');
            const vsSuite: TestSuiteInfoInner = {
                type: 'suite',
                id: suiteId,
                label: suiteLabel,
                file,
                line: suite.lineNumber,
                children: [],
                path: suitePath,
            };
            suitesMap.set(suiteId, vsSuite);
        }

        const evaluations = response.evaluations;
        const testsMap = new Map<string, TestInfoInner>();
        for (const e of evaluations) {
            if (e.evaluationType == model.EvaluationType.EVALUATE) {
                const path = e.suite || [];
                const testId = path.concat([e.name]).join('.');
                const vsTest: TestInfoInner = {
                    type: 'test',
                    id: testId,
                    label: e.name,
                    line: e.lineNumber,
                    file,
                    path
                };
                testsMap.set(testId, vsTest);
            }
        }


        const allItems: ItemType[] = [
            ...suitesMap.values(),
            ...testsMap.values()
        ];
        allItems.sort((a, b) => a.line - b.line);

        for (const item of allItems) {
            let parentItemPath;
            const path = item.path;
            if (item.type == 'test') {
                parentItemPath = path
            } else {
                parentItemPath = path.slice(0, path.length - 1)
            }

            let parentItem;
            if (parentItemPath.length == 0) {
                parentItem = root;
            } else {
                const parentItemId = parentItemPath.join('.');
                parentItem = suitesMap.get(parentItemId);
            }

            parentItem.children.push(item);
        }

        return root;
    }

    async run(tests: string[], emitter: EmitterType): Promise<void> {
        emitter.fire({
            type: 'suite',
            suite: 'root',
            state: 'running'
        });
        for (const test of tests) {
            emitter.fire({
                type: 'test',
                test,
                state: 'running'
            });
        }
        const response = await this.request(false, tests);
        for (const e of response.evaluations) {
            let state = undefined;
            let message;
            const value = e.value;
            if (e.evaluationType == model.EvaluationType.ERROR) {
                const input = EvaluateContentProvider.formatEvaluateInput(e.input);
                state = 'errored';
                message = `${input}\n${value}\n`;
            } else if (e.evaluationType == model.EvaluationType.EVALUATE) {
                const input = EvaluateContentProvider.formatEvaluateInput(e.input);
                state = e.testStatus ? e.testStatus : 'passed';
                message = `${input}\n_ :: ${e.type}\n${value}\n`;
            }
            if (state) {
                const suite = e.suite || [];
                const test = suite.concat([e.name]).join('.');
                emitter.fire({
                    type: 'test',
                    test,
                    state,
                    message,
                    tooltip: value,
                    description: value,
                });
            }
        }
        emitter.fire({
            type: 'suite',
            suite: 'root',
            state: 'completed'
        });
    }
}
