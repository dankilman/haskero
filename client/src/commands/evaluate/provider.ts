import * as vscode from 'vscode';
import * as model from './model';
import {HaskeroClient} from '../../utils/haskeroClient';

export const scheme = 'hsi';

export class EvaluateContentProvider implements vscode.TextDocumentContentProvider {
    public readonly scheme: string = scheme;

    constructor(private readonly haskeroClient: HaskeroClient) {}

    onDidChangeEmitter = new vscode.EventEmitter<vscode.Uri>();
	onDidChange = this.onDidChangeEmitter.event;

	async provideTextDocumentContent(uri: vscode.Uri): Promise<string> {
		const evaluationRequests = model.EvaluationRequests.fromUri(uri);
        const response = await this.haskeroClient.client.sendRequest<model.EvaluationResponse>(
            'evaluate', evaluationRequests
		);
		const lines: string[] = [];
		for (const e of response.evaluations) {
			let line: string;
			if (e.evaluationType == model.EvaluationType.DISPLAY) {
				line = `${e.input.join('\n')}\n`;
			} else if (e.evaluationType == model.EvaluationType.EVALUATE) {
				const input = EvaluateContentProvider.formatEvaluateInput(e.input);
				line = `${input}\n_ :: ${e.type}\n${e.value}\n`;
			} else if (e.evaluationType == model.EvaluationType.ERROR) {
				const input = EvaluateContentProvider.formatEvaluateInput(e.input);
				line = `${input}\n${e.value}\n`;
			} else {
				throw new Error(`Invalid request type: ${e.evaluationType}`);
			}
			lines.push(line);
		}
		return lines.join('\n');
	}

	public static formatEvaluateInput(input: string[]): string {
		if (input.length == 0) {
			return '> ???';
		}
		const lines = [`> ${input[0]}`];
		for (const line of input.slice(1)) {
			lines.push(`| ${line}`);
		}
		return lines.join('\n');
	}
}
