import * as vscode from 'vscode';
import * as model from './model';

class Display {
	async text(evaluationRequests: model.EvaluationRequests) {
		const uri = evaluationRequests.toUri();
		await vscode.commands.executeCommand('vscode.setEditorLayout', {
			orientation: 0,
			groups: [
				// vertical
				{ groups: [{}, {}], size: 0.5 },

				// horizontal
				// { size: 0.5 },
				// { size: 0.5 },
			]
		});
		const editor = await vscode.window.showTextDocument(uri, {
			preserveFocus: true,
			viewColumn: vscode.ViewColumn.Two,
		});
		await vscode.languages.setTextDocumentLanguage(editor.document, 'haskell');
	}
}

export default new Display();
