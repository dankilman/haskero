import * as vscode from 'vscode';

class Code {
	private readonly defaultText = '';

    get editor() {
        return vscode.window.activeTextEditor;
    }

	async queryInput(): Promise<string> {
		return vscode.window.showInputBox({
			placeHolder: 'e.g. 1 + 1',
			prompt: 'Evaluate In Module'
		});
	}

	getTextInContext(): string {
		const editor = this.editor;
		if (!editor) {
			return this.defaultText;
		}
		const editorDocument = editor.document;
		const editorSelection = editor.selection;
		let range: vscode.Range = editorSelection;
		if (editorSelection.isEmpty) {
			const lineNum = editorSelection.active.line;
			range = editorDocument.lineAt(lineNum).range;
		}
		let text = editorDocument.getText(range);
		if (text.length === 0) {
			text = this.defaultText;
		}
		return text;
	}

	getText(): string {
		const editor = this.editor;
		if (!editor) {
			return this.defaultText;
		}
		return editor.document.getText();
	}

	getCurrentFilename(): string {
		const editor = this.editor;
		if (!editor) {
			return this.defaultText;
		}
		return editor.document.fileName;
	}

}

export default new Code();
