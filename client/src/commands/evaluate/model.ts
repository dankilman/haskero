import * as vscode from 'vscode'


export class EvaluationRequests {
	constructor(
		public readonly evaluationRequests: string,
		public readonly onlyTestCases: boolean,
		private readonly scheme?: string,
		public readonly onlyLoad?: boolean,
		public readonly tests?: string[],
    ) {}

	toUri(): vscode.Uri {
		const base = vscode.Uri.parse(`${this.scheme}:evaluate`);
		return base.with({query: this.toQueryString(), fragment: Math.random().toString()});
	}

	static fromUri(uri: vscode.Uri): EvaluationRequests {
		return EvaluationRequests.fromQueryString(uri.query);
	}

	private toQueryString(): string {
		return encodeURI(JSON.stringify(this));
	}

	private static fromQueryString(queryString: string): EvaluationRequests {
		return JSON.parse(decodeURI(queryString));
	}
}

export enum EvaluationType {
    EVALUATE,
	DISPLAY,
	ERROR,
}

export interface Suite {
    readonly path: string[],
    readonly lineNumber: number,
}

export interface Evaluation {
	readonly name: string;
	readonly input: string[];
	readonly evaluationType: EvaluationType;
	readonly value?: string;
	readonly type?: string;
    readonly lineNumber?: number;
    readonly testStatus?: string;
    readonly suite?: string[];
}

export interface EvaluationResponse {
	readonly evaluations: Evaluation[];
	readonly suites: Suite[];
}
