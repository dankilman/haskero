import {HaskeroClient} from '../utils/haskeroClient';
import display from './evaluate/display';
import code from './evaluate/code';
import {scheme} from './evaluate/provider'
import * as model from './evaluate/model'

abstract class BaseEvaluate {
	constructor(private readonly _: HaskeroClient) {}

	public async handler() {
		await this.handle(await this.getText(), this.onlyTestCases);
    }

    protected abstract getText(): Promise<string>;

	protected get onlyTestCases(): boolean {
		return false;
	}

	private async handle(text: string, onlyTestCases: boolean) {
		if (!text) {
			return;
		}
		const evaluationRequests = new model.EvaluationRequests(
			text,
			onlyTestCases,
			scheme,
		);
		await display.text(evaluationRequests);
	}
}

export class Evaluate extends BaseEvaluate {
	public readonly id: string = 'haskero.evaluate';

	protected async getText(): Promise<string> {
		return code.getTextInContext();
	}
}

export class EvaluateFromInput extends BaseEvaluate {
	public readonly id: string = 'haskero.evaluateFromInput';

	protected getText(): Promise<string> {
		return code.queryInput()
	}
}

export class EvaluateTestCases extends BaseEvaluate {
	public readonly id: string = 'haskero.evaluateTestCases';

	protected async getText(): Promise<string> {
		return code.getText();
	}

	protected get onlyTestCases(): boolean {
		return true;
	}
}
