import * as path from 'path';
import * as vscode from 'vscode';
import { TestHub, testExplorerExtensionId } from 'vscode-test-adapter-api';
import { Log, TestAdapterRegistrar } from 'vscode-test-adapter-util';
import {InsertTypeAbove} from './commands/insertTypeAbove';
import {SelectTarget} from './commands/selectTargets';
import {Evaluate, EvaluateFromInput, EvaluateTestCases} from './commands/evaluate';
import {EvaluateContentProvider} from './commands/evaluate/provider';
import {HaskeroClient, HaskeroSettings} from './utils/haskeroClient';
import {HaskeroAdapter} from "./testExplorer/adapter";

export async function activate(context: vscode.ExtensionContext) {
    const serverModule = context.asAbsolutePath(path.join('server', 'server.js'));
    const haskeroClient = new HaskeroClient(serverModule, isDebug());
    haskeroClient.start({
        settings: getSettings(),
        targets: []
    });
    await haskeroClient.client.onReady();
    registerCommands(haskeroClient, context);
    registerContentProviders(haskeroClient, context);
    registerTestExplorerTestAdapter(haskeroClient, context);
    createTargetSelectionButton(context);
    context.subscriptions.push(haskeroClient);
}

function df<T>(value: T, defaultValue: T): T {
    return (value === null || value === undefined) ? defaultValue : value;
}

function isDebug() {
    return df(<boolean>vscode.workspace.getConfiguration('haskero').get('debugMode'), false);
}

function getSettings(): HaskeroSettings {
    return {
        intero: {
            stackPath: df(<string>vscode.workspace.getConfiguration('haskero.intero').get('stackPath'), 'stack'),
            startupParams: df(<[string]>vscode.workspace.getConfiguration('haskero.intero').get('startupParams'), ['--no-build', '--no-load']),
            ghciOptions: df(<[string]>vscode.workspace.getConfiguration('haskero.intero').get('ghciOptions'), ['-Wall']),
            ignoreDotGhci: df(<boolean>vscode.workspace.getConfiguration('haskero.intero').get('ignoreDotGhci'), true)
        },
        debugMode: isDebug(),
        maxAutoCompletionDetails: df(<number>vscode.workspace.getConfiguration('haskero').get('maxAutoCompletionDetails'), 100)
    };
}

function registerCommands(haskeroClient: HaskeroClient, context: vscode.ExtensionContext) {
    const cmds = [
        new InsertTypeAbove(haskeroClient),
        new SelectTarget(haskeroClient),
        new Evaluate(haskeroClient),
        new EvaluateFromInput(haskeroClient),
        new EvaluateTestCases(haskeroClient),
    ];
    for (const cmd of cmds) {
        context.subscriptions.push(vscode.commands.registerCommand(cmd.id, cmd.handler.bind(cmd)));
    }
}

function registerContentProviders(haskeroClient: HaskeroClient, context: vscode.ExtensionContext) {
    const providers = [
        new EvaluateContentProvider(haskeroClient),
    ];
    for (const provider of providers) {
        context.subscriptions.push(vscode.workspace.registerTextDocumentContentProvider(
            provider.scheme, provider
        ));
    }
}

function registerTestExplorerTestAdapter(haskeroClient: HaskeroClient, context: vscode.ExtensionContext) {
	const workspaceFolder = (vscode.workspace.workspaceFolders || [])[0];
	const log = new Log('haskeroExplorer', workspaceFolder, 'Haskero Explorer Log');
	context.subscriptions.push(log);
	const testExplorerExtension = vscode.extensions.getExtension<TestHub>(testExplorerExtensionId);
	if (log.enabled) {
		log.info(`Test Explorer ${testExplorerExtension ? '' : 'not '}found`);
	}
	if (!testExplorerExtension) {
	    return;
    }
    const testHub = testExplorerExtension.exports;
    context.subscriptions.push(new TestAdapterRegistrar(
        testHub,
        _ => new HaskeroAdapter(haskeroClient, log),
        log
    ));
}

function createTargetSelectionButton(context: vscode.ExtensionContext) {
    const barItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, Number.MIN_VALUE);
    barItem.text = "Targets: default";
    barItem.command = SelectTarget.id;
    barItem.show();
    context.subscriptions.push(
        SelectTarget.onTargetsSelected.event((haskeroTargets) => {
            barItem.text = haskeroTargets.toText();
        })
    );
}
