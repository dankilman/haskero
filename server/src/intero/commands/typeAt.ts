import { InteroProxy } from '../interoProxy'
import { InteroUtils } from '../interoUtils'
import { InteroRequest } from './interoRequest'
import { InteroResponse } from './interoResponse'
import { InteroRange } from '../interoRange'
import { UriUtils } from '../../utils/uriUtils'

/**
 * type-at intero response
 */
export class TypeAtResponse implements InteroResponse {

    public readonly isOk: boolean = true;
    public readonly type: string;

    public constructor(public readonly rawout: string, public readonly rawerr: string, private infoKind: TypeInfoKind, private id?: string) {
        this.type = InteroUtils.normalizeRawResponse(rawout);
        if (this.type && this.type.length > 0) {
            //if the instantiated info kind is used, intero doesn't responds the identifier name, so we add it
            if (infoKind === TypeInfoKind.Instanciated) {
                this.type = id + ' ' + this.type;
            }
        }
    }
}

/**
 * type-at intero request
 */
export class TypeAtRequest implements InteroRequest<TypeAtResponse> {
    public constructor(private uri: string, private range: InteroRange, private identifier: string, private infoKind: TypeInfoKind) {
    }

    public async send(interoProxy: InteroProxy): Promise<TypeAtResponse> {
        const filePath = UriUtils.toFilePath(this.uri);
        const escapedFilePath = InteroUtils.escapeFilePath(filePath);
        //if we add the identifier to the request, intero responds the more generic type signature possible
        //if we dont add the identifier to the request, intero responds the more specific type signature, as used in the specific text span
        const id = this.infoKind === TypeInfoKind.Generic ? ' ' + this.identifier : '';
        const req = `:type-at ${escapedFilePath} ${this.range.startLine} ${this.range.startCol} ${this.range.endLine} ${this.range.endCol}${id}`;
        const response = await interoProxy.sendRawRequest(req);
        return new TypeAtResponse(response.rawout, response.rawerr, this.infoKind, this.identifier);
    }
}


/**
 * Kind of type info, the generic or the instantiated on
 */
export enum TypeInfoKind {
    /**
     * Specialized type info, for a specific usage with closed/instantiated types (used in type hover for instance)
     */
    Instanciated,
    /**
     * Generic type signature (used in type insert for instance)
     */
    Generic
}
