import {InteroProxy} from '../interoProxy'
import {InteroRequest} from './interoRequest'
import {InteroResponse} from './interoResponse'
import {
    Evaluation,
    EvaluationRequestParams,
    EvaluationType,
    ParsedEvaluationRequest,
    Suite
} from "./evaluate/model";
import parser from "./evaluate/parser";

export class EvaluationResponse implements InteroResponse {
    isOk: boolean = true;
    rawout: string = '';
    rawerr: string = '';
    public constructor(
        public readonly evaluations: Evaluation[],
        public readonly suites: Suite[],
    ) {}
}

export class EvaluationRequest implements InteroRequest<EvaluationResponse> {
    public constructor(private readonly params: EvaluationRequestParams) {}

    async send(interoProxy: InteroProxy): Promise<EvaluationResponse> {
        const parsedRequests = parser.parse(
            this.params.evaluationRequests,
            this.params.onlyTestCases,
        );
        const evaluations: Evaluation[] = [];
        for (const evaluationRequest of parsedRequests.requests) {
            const {lines, name, lineNumber, evaluationType, suite} = evaluationRequest;
            if (!this.shouldProcess(name, suite)) {
                continue;
            }
            if (evaluationType == EvaluationType.DISPLAY) {
                evaluations.push({
                    input: lines,
                    evaluationType,
                    lineNumber,
                    name,
                })
            } else if (evaluationType == EvaluationType.EVALUATE) {
                if (this.params.onlyLoad) {
                    evaluations.push({
                        input: lines,
                        evaluationType,
                        lineNumber,
                        name,
                        suite,
                    });
                } else {
                    try {
                        evaluations.push(await this.evaluate(interoProxy, evaluationRequest));
                    } catch (error) {
                        const value = `${error}`;
                        evaluations.push({
                            input: lines,
                            value,
                            evaluationType: EvaluationType.ERROR,
                            lineNumber,
                            name,
                        });
                    }
                }
            } else {
                throw new Error(`Invalid evaluation type: ${evaluationType}`);
            }
        }
        return new EvaluationResponse(evaluations, parsedRequests.suites);
    }

    private shouldProcess(name: string, suite: string[]): boolean {
        suite = suite || [];
        const tests = this.params.tests;
        if (!tests) {
            return true;
        }
        const paths = suite.concat([name]);
        for (let i = 1; i < paths.length + 1; i++) {
            const currentPath = paths.slice(0, i);
            const currentId = currentPath.join('.');
            if (tests.includes(currentId)) {
                return true;
            }
        }
        return false;
    }

    private async evaluate(
        proxy: InteroProxy,
        {lines, testDescriptor, lineNumber, name, suite}: ParsedEvaluationRequest): Promise<Evaluation> {
        const response = await proxy.sendMultilineRequest(lines);
        response.throwIfError();
        const outLines = response.rawout.trim().split('\n');
        const rawType = outLines.pop();
        const rawTypeSplit = rawType.split(' :: ');
        rawTypeSplit.shift();
        const type = rawTypeSplit.join(' :: ');
        const value = outLines.join('\n');
        let testStatus = undefined;
        const passed = 'passed';
        const failed = 'failed';
        if (testDescriptor.bool) {
            testStatus = value == 'True' ? passed : failed;
        } else if (testDescriptor.quickCheck) {
            testStatus = value.includes(' OK') ? passed : failed;
        }
        return {
            input: lines,
            type,
            value,
            lineNumber,
            name,
            suite,
            testStatus,
            evaluationType: EvaluationType.EVALUATE,
        };
    }
}
