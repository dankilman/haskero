export interface InteroResponse {
    /**
     * The response state
     */
    isOk: boolean;

    /**
     * Text received on stderr
     */
    readonly rawerr: string;

    /**
     * Text received on stdout
     */
    readonly rawout: string;
}
