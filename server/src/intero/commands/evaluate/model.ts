export enum EvaluationType {
    EVALUATE,
    DISPLAY,
    ERROR,
}

export interface TestDescriptor {
    readonly bool: boolean;
    readonly quickCheck: boolean;
}

export interface Suite {
    readonly path: string[],
    readonly lineNumber: number,
    readonly index: number,
}

export interface ParsedEvaluationRequests {
    readonly suites: Suite[];
    readonly requests: ParsedEvaluationRequest[];
}

export interface ParsedEvaluationRequest {
    readonly lines: string[];
    readonly evaluationType: EvaluationType;
    readonly lineNumber: number;
    readonly name: string;
    readonly isTestCase: boolean;
    readonly suite: string[];
    readonly testDescriptor: TestDescriptor;
}

export interface Evaluation {
    readonly name: string;
    readonly input: string[];
    readonly evaluationType: EvaluationType;
    readonly value?: string;
    readonly type?: string;
    readonly lineNumber: number;
    readonly testStatus?: string;
    readonly suite?: string[];
}

export interface EvaluationRequestParams {
	readonly evaluationRequests: string;
	readonly onlyTestCases: boolean;
    readonly onlyLoad: boolean;
    readonly tests?: string[];
}
