import {
    EvaluationType,
    ParsedEvaluationRequest,
    ParsedEvaluationRequests,
    Suite,
    TestDescriptor
} from "./model";

class TestDescriptorImpl implements TestDescriptor {
    public readonly name?: string;
    public readonly bool: boolean = false;
    public readonly quickCheck: boolean = false;

    constructor(public readonly rawDescriptor: string) {
        const parts = rawDescriptor.trim().split(' ');
        for (const part of parts) {
            if (part.startsWith("'")) {
                this.name = part.substring(1, part.length);
            }
            if (part == '?') {
                this.bool = true;
            }
            if (part.toLocaleLowerCase() == 'qc') {
                this.quickCheck = true;
            }
        }
    }
}

class CurrentExpression {
    public expression?: string[];
    public lineNumber?: number;
    public descriptor: TestDescriptorImpl = new TestDescriptorImpl('');
    public isTestCase: boolean = false;
}

interface AddLineOptions {
    exp: string;
    lineNumber: number;
    descriptor: TestDescriptorImpl;
    isTestCase: boolean;
}

class ParseContext {
    private requests: ParsedEvaluationRequest[] = [];
    private currentExpression = new CurrentExpression();
    private suites: Suite[] = [];
    private suitePath: Suite[] = [];

    processSuiteDescriptor(lineNumber: number, desc: string) {
        const parts = desc.trim().split(' ');
        const suiteNesting = parts[0].length;
        const suiteName = parts.length > 1 ? parts[1] : undefined;

        while (suiteNesting < this.suitePath.length) {
            this.suitePath.pop();
        }

        let index = 0;
        if (suiteNesting == this.suitePath.length) {
            const currentSuite = this.suitePath.pop();
            index = currentSuite.index + 1;
        }

        let path = [suiteName || index.toString()];
        if (this.suitePath.length > 0) {
            path = this.suitePath[this.suitePath.length - 1].path.concat(path);
        }

        this.suites.push({path, lineNumber, index});
        this.suitePath.push({path, lineNumber, index});
    }

    addLine(options: AddLineOptions) {
        if (!this.currentExpression.lineNumber) {
            this.currentExpression.lineNumber = options.lineNumber;
        }
        if (!this.currentExpression.expression) {
            this.currentExpression.expression = [];
        }
        if (options.descriptor && options.descriptor.rawDescriptor) {
            this.currentExpression.descriptor = options.descriptor
        }
        if (options.isTestCase) {
            this.currentExpression.isTestCase = options.isTestCase;
        }
        this.currentExpression.expression.push(options.exp);
    }

    get hasExpression(): boolean {
        return !!this.currentExpression.expression;
    }

    completeExpression(evaluationType: EvaluationType) {
        this.requests.push({
            lines: this.currentExpression.expression,
            lineNumber: this.currentExpression.lineNumber,
            name: this.currentExpression.descriptor.name || this.currentExpression.expression.join('\n'),
            testDescriptor: this.currentExpression.descriptor,
            isTestCase: this.currentExpression.isTestCase,
            evaluationType,
            suite: this.suitePath.length > 0 ? this.suitePath[this.suitePath.length - 1].path : undefined
        });
        this.currentExpression = new CurrentExpression();
    }

    finishAndGetRequests(): ParsedEvaluationRequests {
        if (this.hasExpression) {
            this.completeExpression(EvaluationType.EVALUATE);
        }
        return {
            requests: this.requests,
            suites: this.suites
        };
    }
}

class TextParser {

    private readonly testPrefix = '_= ';
    private readonly testDescriptorRe = /-- (.*)/;

	parse(text: string, onlyTestCases: boolean): ParsedEvaluationRequests {
	    const parsedRequests = this.collectRequests(text);
	    return {
	        suites: parsedRequests.suites,
            requests: parsedRequests.requests.filter(r => !onlyTestCases || r.isTestCase)
        };
    }

    private collectRequests(text: string): ParsedEvaluationRequests {
        const context = new ParseContext();
        let currentLine = -1;
        for (let line of text.split('\n')) {
            currentLine++;

            if (line.startsWith('-- #')) {
                if (context.hasExpression) {
                    context.completeExpression(EvaluationType.EVALUATE);
                }
                const desc = line.substring('-- '.length, line.length);
                context.processSuiteDescriptor(currentLine, desc);
                continue;
            }

            let isTestCase = false;
            let descriptor = null;
            if (line.startsWith(this.testPrefix)) {
                line = line.substring(this.testPrefix.length, line.length);
                isTestCase = true;
                const descriptorTestCase = this.testDescriptorRe.exec(line.trim());
                if (descriptorTestCase) {
                    line = line.substr(0, descriptorTestCase.index);
                    descriptor = new TestDescriptorImpl(descriptorTestCase[1]);
                }
            }

            if (!isTestCase && !line.trim()) {
				continue;
            }

            if (!this.isIndented(line) && context.hasExpression) {
                context.completeExpression(EvaluationType.EVALUATE);
            }

            context.addLine({exp: line, lineNumber: currentLine, descriptor, isTestCase});

            if (line.startsWith('--')) {
                context.completeExpression(EvaluationType.DISPLAY);
            }
        }
		return context.finishAndGetRequests();
    }

    private isIndented(line: string) {
        return line.length > 0 && line.charAt(0) == ' ';
    }

}

export default new TextParser();
