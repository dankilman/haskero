export function zipWith<TX, TY, TResult>(
    xs: Array<TX>,
    ys: Array<TY>,
    combine: (TX, TY) => TResult): Array<TResult> {

    const result = new Array<TResult>();
    for (let i = 0; i < xs.length && i < ys.length; i++) {
        const x = xs[i];
        const y = ys[i];
        result.push(combine(x, y));
    }
    return result;
}
